/* Madeleine Lawrie
mlawrie
Lab 1
001
Kathy and Nushrat 
*/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };
//values 2-14
typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
  Card Array[52]; 

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
	int i;
	for(i = 0; i < 13; i++) { //for loop for spades
		Array[i].suit = SPADES;
		Array[i].value = i + 2; 
	}
	for(i = 13; i < 26; i++) { //for loop for hearts
		Array[i].suit = HEARTS;
		Array[i].value = (i % 13) + 2; 
	}
	for(i = 26; i < 39; i++) { //for loop for diamonds
		Array[i].suit = DIAMONDS;
		Array[i].value = (i % 26) + 2; 
	}
	for(i = 39; i < 52; i++) { //for loop for clubs
		Array[i].suit = CLUBS;
		Array[i].value = (i % 39) + 2;
   	}
   //random shuffle has a pointer to beginning of array, 1 past the end, and 1 to the function
   random_shuffle(&Array[0], &Array[52], *myrandom);
   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
   Card handOfCards[5] = {Array[0], Array[1], Array[2], Array[3], Array[4]};

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/ 
   //has a pointer to beginning of array, 1 past the end, and 1 to the function suit_order
   sort(&Array[0], &Array[52], suit_order); 
    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
    int j;
    //width of 10 and right aligned, using I/O manipulators, each card on own line
    for(j = 0; j < 5; j++) {
	cout << setw(10) << right << get_card_name(handOfCards[j]) << " of " << get_suit_code(handOfCards[j]) << endl;
    }

  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  // takes in 2 cards and returns true if lhs < rhs and false otherwise..
  //if they are the same, compare card value. Return true if lhs < rhs
  // IMPLEMENT
  if (lhs.suit < rhs.suit) { 
	return true;
  }
  else if(lhs.suit == rhs.suit) {
	return lhs.value < rhs.value;
  } 
  else {
	return false;
  }
}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  // IMPLEMENt
  //similar to get_suit_card
  //cards value is used to return string name
  switch (c.value) {
    case 2:    return "2";
    case 3:    return "3";
    case 4:    return "4";
    case 5:    return "5";
    case 6:    return "6";
    case 7:    return "7";
    case 8:    return "8";
    case 9:    return "9";
    case 10:   return "10";
    case 11:   return "Jack";
    case 12:   return "Queen";
    case 13:   return "King";
    case 14:   return "Ace";
    default:   return "";
  }
}

